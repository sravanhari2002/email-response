import openai
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# API Configurations
# openai.api_type = "azure"
# openai.api_base = "https://sravanakumar13sathish.openai.azure.com/"
# openai.api_key = "f93979cbf9894257affd4fee8b4e08fb"
# openai.api_version = "2023-03-15-preview"
# deployment_name = 'Policy_GPT'
openai.api_type = "azure"
openai.api_key = "e49e5e1a1fea4ffe8c2e39066a3ffebf"
openai.api_base = "https://maninthomas.openai.azure.com/"
openai.api_version = "2023-03-15-preview"
deployment_name = 'text-davinci-003'

def run_completion(prompt: str, deployment_name: str, temperature=0.7, max_tokens=100, verbose=False):
    try:
        # Create a completion for the provided prompt and parameters
        # To know more about the parameters, check out this documentation: https://learn.microsoft.com/en-us/azure/cognitive-services/openai/reference
        completion = openai.Completion.create(
            prompt=prompt,
            temperature=temperature,
            max_tokens=max_tokens,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0,
            engine=deployment_name
        )

        # Print the completion
        if verbose:
            print(completion.choices[0].text.strip(" \n"))

        return completion.choices[0].text

        # Check if the response is filtered
        if completion.choices[0].finish_reason == "content_filter":
            print("The generated content is filtered.")

    except openai.error.APIError as e:
        # Handle API error here, e.g. retry or log
        print(f"OpenAI API returned an API Error: {e}")

    except openai.error.AuthenticationError as e:
        # Handle Authentication error here, e.g. invalid API key
        print(f"OpenAI API returned an Authentication Error: {e}")

    except openai.error.APIConnectionError as e:
        # Handle connection error here
        print(f"Failed to connect to OpenAI API: {e}")

    except openai.error.InvalidRequestError as e:
        # Handle connection error here
        print(f"Invalid Request Error: {e}")

    except openai.error.RateLimitError as e:
        # Handle rate limit error
        print(f"OpenAI API request exceeded rate limit: {e}")

    except openai.error.ServiceUnavailableError as e:
        # Handle Service Unavailable error
        print(f"Service Unavailable: {e}")

    except openai.error.Timeout as e:
        # Handle request timeout
        print(f"Request timed out: {e}")

def Generate_Email(recipient_mail, answer):
    smtp_server = "smtp.gmail.com"
    port = 587  # TLS Port
    sender_email = "sravanhari2002@gmail.com"
    password = 'zdcvqizbrkimxqwc'
    subject = "Answer"
    message = f"Hi sir/ma'am,\nHere is the answer for the question you asked.\n{answer}\n\nThank You!"

    msg = MIMEMultipart()
    msg["From"] = sender_email
    msg["To"] = recipient_mail
    msg["Subject"] = subject

    msg.attach(MIMEText(message, 'plain'))

    try:
        server = smtplib.SMTP(smtp_server, port)
        server.starttls()
        server.login(sender_email, password)
        text = msg.as_string()
        server.sendmail(sender_email, recipient_mail, text)
    except Exception as e:
        print(f"Error: {e}")
    finally:
        server.quit()


def generate_answer(content, question, recipient_mail):
    # Combine the prompt and the question
    prompt = content + "\n\n" + question

    # Call the function to generate the answer
    answer = run_completion(prompt, deployment_name, temperature=1.0, max_tokens=1000, verbose=False)

    # Generate and send the email
    Generate_Email(recipient_mail, answer)

    return answer
