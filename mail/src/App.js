import React, { useState } from 'react';
import './App.css';
import axios from 'axios';

const Homepage = () => {
  const [mailContent, setMailContent] = useState('');
  const [question, setQuestion] = useState('');
  const [recipientEmail, setRecipientEmail] = useState('');

  const [output, setOutput] = useState(false)
  const [hasSent, setHasSent] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault();
    // Handle form submission logic here
    let data = JSON.stringify({
      "content": mailContent,
      "question": question,
      "recipient_mail": recipientEmail
    });
    
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'http://localhost:5000/requesting',
      headers: { 
        'Content-Type': 'application/json'
      },
      data : data
    };
    
    axios.request(config)
    .then((response) => {
      setOutput(response.data.message)
      setHasSent(true)
      setMailContent('');
    setQuestion('');
    setRecipientEmail('');
      console.log(JSON.stringify(response.data));
    })
    .catch((error) => {
      console.log(error);
    });
    
// await fetch("http://127.0.0.1:5000/requesting", {
//   method: 'post',
//   withCredentials: false,
//   mode: 'no-cors',
//   body: {
//     "content": mailContent,
//     "question": question,
//     "recipient_mail": question
//   },
// })
//   .then(response => response.text())
//   .then(result => console.log(result))
//   .catch(error => console.log('error', error));
//     // Reset input values
  };

  const handleClearForm = () => {
    setMailContent('')
    setQuestion('')
    setRecipientEmail('')
  }

  return (
    <div className="homepage">
      <div className="title">
        <h1>Question answer emailing system</h1>
      </div>
        <p className='subheading'>Enter the content, question, and recipient’s email address to generate the answer and send it as an email.</p>

        <div className='formBox'>
        
        <form className="email-form form" onSubmit={handleSubmit}>
          <div className='email-form-object'>
          <label>Content</label>
          <input
          type="text"
          placeholder="Mail Content"
          value={mailContent}
          onChange={(e) => setMailContent(e.target.value)}
        />
          </div>

          <div className='email-form-object'>
            <label>Question</label>
            <input
          type="text"
          placeholder="Question"
          value={question}
          onChange={(e) => setQuestion(e.target.value)}
        />
          </div>

          <div className='email-form-object'>
            <label>Recipeint Email</label>
            <input
          type="email"
          placeholder="Recipient Email"
          value={recipientEmail}
          onChange={(e) => setRecipientEmail(e.target.value)}
        />
          </div>
        
        <div className='formButtons'>
          <button className='clearButton' onClick={handleClearForm}>Clear</button>
          <button className='submitButton'>Submit</button>
        </div>
      </form>

      <form className='form output-form'>
        <div className='outputHeader'>
        <label>
            Output
          </label>
        </div>
          <br />
          <br />
          {output && <label>{output}</label>}
      </form>
        </div>

        {hasSent && <h3 className='successMessage'>The above output has been sent as an email</h3>}
    </div>
  );
};

export default Homepage;
