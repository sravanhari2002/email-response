from flask import Flask, request, jsonify, send_file
import back
from flask_cors import CORS

app = Flask(__name__)

CORS(app)

@app.route('/requesting',methods=['POST'])
def send_request():
    try:
        #if request.headers['Content-Type']=='application/x-www-form-urlencode':
        data=request.json
        content=data.get('content','')
        question=data.get('question','')
        recipient_mail=data.get('recipient_mail','')
        # elif request.headers['Content-Type']=='application/json':
        #     content=request.form['content']
        #     question=request.form['question']
        #     recipient_mail=request.form['recipient_mail']
        # else:
        #    return "Wrong Media"
        ans = back.generate_answer(content, question, recipient_mail)
        return jsonify({"message":ans})
    except Exception as e:
        return jsonify({"error": e})


if __name__=='__main__':
    app.run()